grep -c "that" 1.txt
grep -e "this" -e "to" *.txt
grep -h "to" *.txt
grep -i "document" *.txt
grep -l "is" *.txt
grep -n "is" *.txt
grep -o "this" *.txt
grep -v "this" *.txt
grep -f *.txt
grep -v "this" *.txt
grep -w "is" *.txt
grep "^this" *.txt
