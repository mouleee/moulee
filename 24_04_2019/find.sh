find . -type f -name "*.txt"
find . -type d -name "*"
find . -mtime 1
find . -ctime 1
find . -atime 1
find . -mindepth 4 -maxdepth 7 -type f -iname "*.txt"
find . -mindepth 3 -maxdepth 3 -type f -name "*.txt" -delete
find . -mindepth 1 -maxdepth 7 -type f -not -name "*.txt"
find . -type d -empty
