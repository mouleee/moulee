Then I would look at writing a quick custom task  (they really aren't that hard) so that you can eliminate all the details of the file structure to within the task.

I would see something like:

<getGroupValue filename="..." lookfor="...." setproperty="result"/>

You would pass in (as attributes) the filename, the lookfor value (i.e. 'Test2'), and the name of the property that would be defined whose value will the group that was found.
This is a lot cleaner than exposing/manipulating the file (and it rigid structure) through a series of generic ANT 
Is there anyway to get back the line number where this is found - If I
can get back the line number - I could use the headfilter to get this
next line.
