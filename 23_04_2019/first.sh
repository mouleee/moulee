mkdir -p TOMCAT

url1=http://www-eu.apache.org/dist/tomcat/tomcat-9/v9.0.19/bin/apache-tomcat-9.0.19.zip
url2=http://www-eu.apache.org/dist/tomcat/tomcat-9/v9.0.19/bin/apache-tomcat-9.0.19.tar.gz
url3=http://www-eu.apache.org/dist/tomcat/tomcat-9/v9.0.19/bin/apache-tomcat-9.0.19.exe
url4=http://www-eu.apache.org/dist/tomcat/tomcat-9/v9.0.17/bin/apache-tomcat-9.0.17.zip
url5=http://www-eu.apache.org/dist/tomcat/tomcat-9/v9.0.17/bin/apache-tomcat-9.0.17.tar.gz
url6=http://www-eu.apache.org/dist/tomcat/tomcat-9/v9.0.17/bin/apache-tomcat-9.0.17.exe

wget $url1
wget $url2
wget $url3
wget $url4
wget $url5
wget $url6

mkdir -p TOMCAT/zips_parallel
mv *.zip TOMCAT/zips_parallel

for i in $url1 $url2 $url3 $url4 $url5 $url6
do
wget $i
done

mkdir -p TOMCAT/zips_for
mv *.zip TOMCAT/zips_for

ls TOMCAT > ls.txt

mkdir -p TOMCAT/output
mv ls.txt TOMCAT/output

zip -r tomcat_binaries.zip TOMCAT -x "TOMCAT/output/*"
mv tomcat_binaries.zip TOMCAT/output
