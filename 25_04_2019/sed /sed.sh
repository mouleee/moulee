sed 's/unix/linux/' sample.txt
sed 's/unix/linux/2' sample.txt
sed 's/unix/linux/g' sample.txt
sed 's/unix/linux/2g' sample.txt
sed '3 s/unix/linux/' sample.txt
sed '4 s/unix/linux/2g' sample.txt
sed '1,3 s/unix/linux/' sample.txt
sed '2,$ s/unix/linux/' sample.txt
