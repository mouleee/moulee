cat 1.txt | tr "a-z" "A-Z"
cat new.txt | awk '{sum=$1+$2+$3; sum/=3}  {print sum}'
cat 1.txt | tr -d "l"
cat 1.txt | tr -cd [123456789]
cat 1.txt | tr -cd [20]
cat 1.txt | tr -s "potheri" "moulee"
cat 1.txt | tr -t "potheri" "moulee"
cat 1.txt | tr -cd [20]
cut -b 1,2,3 1.txt
cut -b 3-7,9-10 1.txt
cut -c -10 1.txt
cut -d " " -f 1 1.txt
cut -n -d " " -f 1 1.txt 
cut -n -d " " -f 1,2,3 1.txt
ls -halt | awk '{print $6,$7,$8,$9}' 
